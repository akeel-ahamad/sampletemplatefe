import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  items: MenuItem[] = [];
  activeItem: MenuItem = {};
  isUserLoggedIn: boolean = false;

  constructor(
    private router: Router,
  ) {
    this.items = [
      { label: 'Home', icon: 'pi pi-fw pi-home', routerLink: '/home' },
      { label: 'Users', icon: 'pi pi-users', routerLink: '/users' },
      { label: 'Settings', icon: 'pi pi-fw pi-cog', routerLink: '/settings' },
      { label: 'Login', icon: 'pi pi-sign-in', visible: !this.isUserLoggedIn, command: () => { this.loginBtnClicked() } },
      { label: 'Logout', icon: 'pi pi-sign-out', visible: this.isUserLoggedIn, command: () => { this.logoutBtnClicked() } },
    ];

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    ).subscribe((event: any) => {
      if (event.url === "/") {
        this.activeItem = this.items[0];
      } else {
        this.setActiveItemBasedOnCurrentRoute(event.url);
      }
    });

  }

  ngOnInit(): void {

  }

  loginBtnClicked() {
    this.isUserLoggedIn = true;
    this.items[3].visible = !this.isUserLoggedIn;
    this.items[4].visible = this.isUserLoggedIn;
  }

  logoutBtnClicked() {
    this.isUserLoggedIn = false;
    this.items[3].visible = !this.isUserLoggedIn;
    this.items[4].visible = this.isUserLoggedIn;
  }

  setActiveItemBasedOnCurrentRoute(currentRoute: string) {
    let item: any = this.items.find(item => item.routerLink === currentRoute);
    this.activeItem = item;
  }
}
