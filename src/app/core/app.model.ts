
export enum FormMode {
    Add = 'Add',
    Edit = 'Edit',
    Delete = 'Delete'
}

export enum WhichDailog {
    DeleteUser = 'DeleteUser'
}