import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { constants } from '../core/app.constants';
import { WhichDailog } from '../core/app.model';
import { SharedService } from '../core/shared.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private sharedService: SharedService) {
  }

  confirmDialogMsg: string = '';


  ngOnInit() {
    if ((this.config.data.whichDailog === WhichDailog.DeleteUser)) {
      this.confirmDialogMsg = constants.confirmDialogMsg;
    }
  }

  confirm(confirmation: boolean) {

    if (confirmation && this.config.data.whichDailog === WhichDailog.DeleteUser) {
      this.ref.close(true);
    }

    this.ref.close(false);

  }
}
