import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { Subscription } from 'rxjs';
import { FormMode } from 'src/app/core/app.model';
import { SharedService } from 'src/app/core/shared.service';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  private addUserSubscription: Subscription;
  private editUserSubscription: Subscription;

  formMode: string = "";
  user: any = {};
  userForm: FormGroup;
  profilePic: any = null;
  formData = new FormData();
  uploadedFiles: any = [];

  constructor(private fb: FormBuilder, public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private sharedService: SharedService, private usersService: UsersService) {

    this.formMode = this.config.data.mode;
    this.user = this.config.data.data;

    if (this.formMode === FormMode.Add) {
      this.userForm = this.fb.group({
        name: ['', [Validators.required]],
        contactNo: ['', [Validators.required]],
        email: ['', [Validators.required]],
      });
    } else if (this.formMode === FormMode.Edit) {
      this.userForm = this.fb.group({
        name: [this.user.name, [Validators.required]],
        contactNo: [this.user.contactNo, [Validators.required]],
        email: [this.user.email, [Validators.required]],
      });
    }
  }

  ngOnInit(): void {

  }


  /**
   * add or edit user
   * */
  public addUser(): any {

    // this.formData.append("name", this.userForm.value.name);
    // this.formData.append("contactNo", this.userForm.value.contactNo);
    // this.formData.append("email", this.userForm.value.email);
    // if (this.profilePic) {
    //   this.formData.append("profilePic", this.profilePic, this.profilePic.name);
    // }

    // console.log("form data " + this.formData.get("profilePic"));

    let user: any = {
      name: this.userForm.value.name,
      contactNo: this.userForm.value.contactNo,
      email: this.userForm.value.email
    }

    this.sharedService.loading = true;
    if (this.formMode === FormMode.Add) {
      this.addUserSubscription = this.usersService.addUser(user).subscribe((result: any) => {
        this.sharedService.loading = false;
        this.ref.close(true);
      }, (err) => {
        this.sharedService.loading = false;
        this.ref.close(false);
        throw (err);
      });
    } else if (this.formMode === FormMode.Edit) {
      user.userId = this.user.userId;
      // this.formData.append("userId", this.user.userId);

      this.editUserSubscription = this.usersService.editUser(user).subscribe((result: any) => {
        this.sharedService.loading = false;
        this.ref.close(true);
      }, (err) => {
        this.sharedService.loading = false;
        this.ref.close(false);
        throw (err);
      });
    }
  }

  onSelectImage(event) {
    for (let file of event.files) {
      console.log(file);
      this.profilePic = file;
    }
  }


  /**
   * Distroy object
   * */
  ngOnDestroy() {
    this.addUserSubscription ? this.addUserSubscription.unsubscribe() : this.addUserSubscription;
    this.editUserSubscription ? this.editUserSubscription.unsubscribe() : this.editUserSubscription;
  }

}
