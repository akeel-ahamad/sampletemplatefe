import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/dynamicdialog';
import { constants } from '../core/app.constants';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { FormMode, WhichDailog } from '../core/app.model';
import { SharedService } from '../core/shared.service';
import { Subscription } from 'rxjs';
import { UsersService } from './users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  private getUsersSubscription: Subscription;
  private getUserSubscription: Subscription;
  private uploadImageSubscription: Subscription;
  private getImageSubscription: Subscription;
  private deleteImageSubscription: Subscription;
  private exportUsersSubscription: Subscription;


  userHeaders: any[] = [];
  users: any[] = [];

  profilePic: any = null;
  formData = new FormData();
  uploadedFiles: any = [];
  retrievedImage: any;


  constructor(public dialogService: DialogService,
    private sharedService: SharedService, private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.userHeaders = [
      { field: 'name', header: 'Name' },
      { field: 'email', header: 'E-Mail' },
      { field: 'contactNo', header: 'Contact No' },
    ];

    setTimeout(() => {
      this.getUsers();
    }, 0);
  }


  controlAddUserDialog() {

    let ref = null;
    let selectedData = {
      mode: FormMode.Add,
      data: ""
    }

    ref = this.dialogService.open(EditUserComponent, {
      header: constants.addUser,
      data: selectedData,
      width: "30%"
    });

    if (ref) {
      ref.onClose.subscribe((response: any) => {
        if (response) {
          this.getUsers();
        }
      });
    }
  }

  controlEditUserDialog(user: any) {

    let ref = null;
    let selectedData = {
      mode: FormMode.Edit,
      data: user
    }

    ref = this.dialogService.open(EditUserComponent, {
      header: constants.editUser,
      data: selectedData,
      width: "30%"
    });

    if (ref) {
      ref.onClose.subscribe((response: any) => {
        if (response) {
          this.getUsers();
        }
      });
    }
  }

  controlDeleteUserDialog(user: any) {

    let ref = null;
    let selectedData = {
      mode: FormMode.Delete,
      whichDailog: WhichDailog.DeleteUser,
      data: user
    }

    ref = this.dialogService.open(DeleteUserComponent, {
      header: constants.deleteUser,
      data: selectedData
    });

    if (ref) {
      ref.onClose.subscribe((response: any) => {
        if (response) {
          this.getUsers();
        }
      });
    }
  }


  /**
  * get users
  * */
  public getUsers(): any {
    this.sharedService.loading = true;
    this.getUsersSubscription = this.usersService.getUsers().subscribe((result: any) => {
      this.users = result;
      this.sharedService.loading = false;
    }, (err) => {
      this.sharedService.loading = false;
      throw (err);
    });
  }


  /**
   * get user
   * */
  public getUser(userId: string): any {
    this.sharedService.loading = true;
    this.getUserSubscription = this.usersService.getUser(userId).subscribe((result: any) => {
      this.sharedService.loading = false;
    }, (err) => {
      this.sharedService.loading = false;
      throw (err);
    });
  }

  uploadImage(event) {
    for (let file of event.files) {
      console.log(file);
      this.profilePic = file;
    }
    this.formData.append("profilePic", this.profilePic, this.profilePic.name);

    this.sharedService.loading = true;
    this.uploadImageSubscription = this.usersService.uploadImage(this.formData).subscribe((result: any) => {
      this.sharedService.loading = false;
    }, (err) => {
      this.sharedService.loading = false;
      throw (err);
    });


  }


  getImage(imageName = "image003.jpg") {
    this.sharedService.loading = true;
    this.getImageSubscription = this.usersService.getImage(imageName).subscribe((result: any) => {
      this.retrievedImage = 'data:image/jpeg;base64,' + result.picByte;
      this.sharedService.loading = false;
    }, (err) => {
      this.sharedService.loading = false;
      throw (err);
    });
  }


  deleteImage(imageName = "image003.jpg") {
    this.sharedService.loading = true;
    this.deleteImageSubscription = this.usersService.deleteImage(imageName).subscribe((result: any) => {
      this.sharedService.loading = false;
    }, (err) => {
      this.sharedService.loading = false;
      throw (err);
    });
  }


  exportUsers() {
    // http://zetcode.com/springboot/servepdf/
    // https://stackoverflow.com/questions/59686660/how-to-send-generated-pdf-document-to-frontend-in-restfull-way-in-spring-boot

  }

  /**
   * Distroy object
   * */
  ngOnDestroy() {
    this.getUsersSubscription ? this.getUsersSubscription.unsubscribe() : this.getUsersSubscription;
    this.getUserSubscription ? this.getUserSubscription.unsubscribe() : this.getUserSubscription;
    this.uploadImageSubscription ? this.uploadImageSubscription.unsubscribe() : this.uploadImageSubscription;
    this.getImageSubscription ? this.getImageSubscription.unsubscribe() : this.getImageSubscription;
    this.deleteImageSubscription ? this.deleteImageSubscription.unsubscribe() : this.deleteImageSubscription;
    this.exportUsersSubscription ? this.exportUsersSubscription.unsubscribe() : this.exportUsersSubscription;


  }

}