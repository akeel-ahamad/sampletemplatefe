import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { Subscription } from 'rxjs';
import { constants } from '../../core/app.constants';
import { WhichDailog } from '../../core/app.model';
import { SharedService } from '../../core/shared.service';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.scss']
})
export class DeleteUserComponent implements OnInit {

  private deleteUserSubscription: Subscription;
  elementDialogConfirmMsg: string = constants.deleteElementDialogConfirmMsg;
  elementType: string = 'User';
  elementName: string = '';
  user: any = {};

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig,
    private sharedService: SharedService, private usersService: UsersService) {
    this.user = this.config.data.data;
    this.elementName = this.user.name;
  }


  ngOnInit(): void {
  }

  deleteElement(confirmation: boolean) {

    if (confirmation && this.config.data.whichDailog === WhichDailog.DeleteUser) {
      this.deleteUser(this.user.userId);
    } else {
      this.ref.close(false);
    }

  }


  /**
  * delete user
  * */
  public deleteUser(userId: any): any {
    this.sharedService.loading = true;
    this.deleteUserSubscription = this.usersService.deleteUser(userId).subscribe((result: any) => {
      this.sharedService.loading = false;
      this.ref.close(true);
    }, (err) => {
      this.sharedService.loading = false;
      this.ref.close(false);
      throw (err);
    });
  }

  /**
   * Distroy object
   * */
  ngOnDestroy() {
    this.deleteUserSubscription ? this.deleteUserSubscription.unsubscribe() : this.deleteUserSubscription;
  }


}
