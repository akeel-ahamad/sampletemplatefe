import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { constants } from '../core/app.constants';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private serverUrl: string = constants.serverUrl;

  constructor(private http: HttpClient) {

  }


  generateHeaders() {
    return { headers: new HttpHeaders({ 'Content-Type': "application/json", 'Access-Control-Allow-Origin': this.serverUrl }) }
  }


  /**
 * get users
 * */
  public getUsers(): any {
    return this.http.get(this.serverUrl + "/user/", this.generateHeaders());
  }


  /**
   * get user
   * */
  public getUser(userId: string): any {
    return this.http.get(this.serverUrl + "/user/" + userId, this.generateHeaders());
  }

  /**
   * add user
   * */
  public addUser(user: any): any {
    return this.http.post(this.serverUrl + "/user/", user, this.generateHeaders());
  }

  /**
  * edit user
  * */
  public editUser(user: any): any {
    return this.http.put(this.serverUrl + "/user/", user, this.generateHeaders());
  }


  /**
  * delete user
  * */
  public deleteUser(userId: any): any {
    return this.http.delete(this.serverUrl + "/user/" + userId, this.generateHeaders());
  }


  /**
   * upload image
   * */
  public uploadImage(image: any): any {
    return this.http.post(this.serverUrl + "/user/uploadImage/", image);
  }


  /**
 * get image
 * */
  public getImage(imageName): any {
    return this.http.get(this.serverUrl + "/user/image/" + imageName, this.generateHeaders());
  }


  /**
  * delete image
  * */
  public deleteImage(imageName: any): any {
    return this.http.delete(this.serverUrl + "/user/image/" + imageName, this.generateHeaders());
  }

}
