import { Component } from '@angular/core';
import { SharedService } from './core/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SampleTemplateAppFE';
  constructor(
    public sharedService: SharedService) {
  }

}
